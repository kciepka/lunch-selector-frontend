import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from "@angular/common/http";
import { Observable } from "rxjs";
import { LoginService } from "./login.service";

@Injectable({
  providedIn: "root"
})
export class HttpAuthInterceptorService implements HttpInterceptor {
  constructor(private login: LoginService) {}

  private isAuthRequest(request) {
    return request.url.includes("/auth");
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // skip interception of authorization requests
    if (this.isAuthRequest(request)) return next.handle(request);

    // if id_token valid append it to request
    if (this.login.hasValidIdToken()) {
      const customReq = request.clone({
        headers: request.headers.set(
          "Authorization",
          "Bearer " + this.login.getTokens().id_token
        )
      });

      return next.handle(customReq);
    }

    // id_token expired

    // if no refresh_token stored user has to sign in explicitly
    if (!this.login.hasRefreshToken()) {
      this.login.signIn();
    }

    // use refresh_token to refresh id_token silently
    this.login.refreshTokens().then((tokens: any) => {
      let idToken = tokens.id_token;
      const customReq = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + idToken)
      });

      return next.handle(customReq);
    });
  }
}
