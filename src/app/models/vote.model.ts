import { User } from "./user.model";
import { DateRange } from "./date-range.model";

export class Vote {
    timestamp: Date;
    places: string[];
    user: User;
    hours: DateRange[];
}