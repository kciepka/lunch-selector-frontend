export class User {
    id: string;
    familyName: string;
    givenName: string;
    picture: string;
}