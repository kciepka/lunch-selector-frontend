import { User } from "./user.model";

export class Match {
    commonPlaces: string[];
    commonDateRange: Date[];
    users: User[];
}