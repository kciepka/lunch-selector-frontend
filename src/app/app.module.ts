import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule, MatCheckboxModule } from "@angular/material";
import { MatCardModule } from "@angular/material/card";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatChipsModule } from "@angular/material/chips";
import { MatIconModule } from "@angular/material/icon";
import { TimepickerModule } from "ngx-bootstrap/timepicker";

import { AppComponent } from "./app.component";
import { NewLunchComponent } from "./new-lunch/new-lunch.component";
import { MenuComponent } from "./menu/menu.component";
import { HistoryComponent } from "./history/history.component";
import { HowtoComponent } from "./howto/howto.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpAuthInterceptorService } from "./http-auth-interceptor.service";
import { AuthComponent } from "./auth/auth.component";
import { SuggestionPipe } from './suggestion.pipe';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { VotePipe } from './vote.pipe';

registerLocaleData(localePl);

const appRoutes: Routes = [
  { path: "auth", component: AuthComponent },
  { path: "new", component: NewLunchComponent },
  { path: "history", component: HistoryComponent },
  { path: "howto", component: HowtoComponent },
  { path: "", component: MenuComponent },
  { path: "**", component: MenuComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NewLunchComponent,
    MenuComponent,
    HistoryComponent,
    HowtoComponent,
    AuthComponent,
    SuggestionPipe,
    VotePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatToolbarModule,
    MatChipsModule,
    MatIconModule,
    TimepickerModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
