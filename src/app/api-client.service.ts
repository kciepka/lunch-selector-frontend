import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class ApiClientService {
  constructor(private http: HttpClient) { }

  getUser() {
    let url = environment.backendUrl + "/user";
    return this.http.get(url).toPromise();
  }

  getVotes() {
    let url = environment.backendUrl + "/votes/today";
    return this.http.get(url).toPromise();
  }

  addVote(vote) {
    let url = environment.backendUrl + "/votes";
    return this.http.post(url, vote).toPromise();
  }

  getMatches() {
    let url = environment.backendUrl + "/matches/today";
    return this.http.get(url).toPromise();
  }
}
