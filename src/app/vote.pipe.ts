import { Pipe, PipeTransform } from '@angular/core';
import { Vote } from './models/vote.model';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'vote'
})
export class VotePipe implements PipeTransform {

  transform(vote: Vote, args?: any): any {
    let startDate = new DatePipe('pl-PL').transform(vote.hours[0].from, "HH:mm");
    let endDate = new DatePipe('pl-PL').transform(vote.hours[0].to, "HH:mm");
    let pos = vote.places.join(", ").lastIndexOf(', ');
    let places = pos != -1 ? vote.places.join(", ").substring(0, pos) + " lub " + vote.places.join(", ").substring(pos + 1) : vote.places.join(", ");
    let text = `${vote.user.givenName} chce ${places} w godzinach ${startDate} - ${endDate}`;
    return text;
  }
}
