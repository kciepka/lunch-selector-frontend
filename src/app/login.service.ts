import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  public signedIn: boolean = this.getTokens();
  constructor(private http: HttpClient) {}

  getTokens() {
    return JSON.parse(localStorage.getItem("tokens"));
  }

  saveTokens(tokens) {
    localStorage.setItem("tokens", JSON.stringify(tokens));
    this.signedIn = true;
  }

  isSignedIn() {
    return this.getTokens() ? true : false;
  }

  hasValidIdToken() {
    return (
      this.getTokens() &&
      new Date(this.getTokens().expiry_date).getTime() > new Date().getTime()
    );
  }

  hasRefreshToken() {
    return this.getTokens() && this.getTokens().refresh_token;
  }

  freshTokens(code: string) {
    let url = environment.backendUrl + "/auth/tokens?code=" + code;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        tokens => {
          this.saveTokens(tokens);
          resolve(tokens);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  refreshTokens() {
    let url = environment.backendUrl + "/auth/refreshTokens";
    let refreshToken = this.getTokens().refresh_token;
    let body = {
      refresh_token: refreshToken
    };
    return new Promise((resolve, reject) => {
      this.http.post(url, body).subscribe(tokens => {
        this.saveTokens(tokens);
        resolve(tokens);
      }, reject);
    });
  }

  signIn() {
    let url = environment.backendUrl + "/auth";
    this.http.get(url).subscribe(
      (res: any) => {
        window.location.href = res.url + "&prompt=consent";
      },
      err => {
        throw err;
      }
    );
  }
}
