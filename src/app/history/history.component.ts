import { Component, OnInit } from '@angular/core';
import { ApiClientService } from '../api-client.service';
import { Vote } from '../models/vote.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public votes: Vote[];

  constructor(private apiClient: ApiClientService) { }

  ngOnInit() {
    this.apiClient.getVotes().then(data => {
      let votes = <Array<Vote>>data;
      this.votes = votes.sort(this.sortByTimestamp);
    });
  }

  private sortByTimestamp(a, b) {
    let dateA = new Date(a.timestamp);
    let dateB = new Date(b.timestamp);

    if (dateA.getTime() > dateB.getTime()) {
      return -1;
    }

    if (dateA.getTime() < dateB.getTime()) {
      return 1;
    }

    return 0;
  }
}
