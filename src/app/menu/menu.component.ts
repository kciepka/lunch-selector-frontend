import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { ApiClientService } from "../api-client.service";
import { Match } from "../models/match.model";
import { User } from "../models/user.model";
import { Vote } from "../models/vote.model";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit {
  public user: User;
  public matches: Match[] = [];
  public votes: Vote[] = [];
  public userVote: Vote;

  constructor(
    public login: LoginService,
    private apiClient: ApiClientService
  ) { }

  ngOnInit() {
    if (!this.login.isSignedIn()) return this.login.signIn();

    Promise.all([
      this.apiClient.getUser(),
      this.apiClient.getMatches(),
      this.apiClient.getVotes()
    ])
      .then(values => {
        this.user = <User>values[0];
        this.matches = (<Array<Match>>values[1])
          .filter(match => {
            // get only relevant matches for current user
            return match.users[0].id == this.user.id || match.users[1].id == this.user.id;
          })
          .filter(match => {
            // get only matches that have something common
            return (match.commonPlaces && match.commonPlaces.length) || (match.commonDateRange && match.commonDateRange.length);
          })
          .map(match => {
            // remove current user from match users
            match.users = match.users.filter(user => { return user.id !== this.user.id });
            return match;
          });
        this.votes = <Array<Vote>>values[2];
        this.userVote = this.votes.filter(vote => { return vote.user.id = this.user.id })[0];
      })
      .catch(err => {
        console.error("Could not load user and matches");
        console.error(err);
      });
  }
}
