import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { ActivatedRoute, Params, Router } from "@angular/router";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"]
})
export class AuthComponent implements OnInit {
  constructor(
    private login: LoginService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      let code = params["code"];
      this.login.freshTokens(code).then(() => {
        this.router.navigate(["/"]);
      });
    });
  }
}
