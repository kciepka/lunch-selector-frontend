import { Component, OnInit } from "@angular/core";
import { ApiClientService } from "../api-client.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-new-lunch",
  templateUrl: "./new-lunch.component.html",
  styleUrls: ["./new-lunch.component.css"]
})
export class NewLunchComponent implements OnInit {

  submitDisabled: boolean = true;
  timeRange = {
    from: new Date(),
    to: new Date(new Date().getTime() + 30 * 60000)
  };
  allRestaurants = [
    { name: "OPSO", id: 1, open: [1, 2, 3, 4, 5], menu: 'http://opso.pl/menu/', selected: false },
    { name: "OPSO Pizza", open: [2, 5], menu: 'http://opso.pl/menu/', id: 2, selected: false },
    { name: "Dobre Smaki", open: [1, 2, 3, 4, 5], id: 3, menu: 'http://dobresmaki.pl/', selected: false },
    { name: "Hindus", open: [2], id: 4, menu: 'http://www.hindusfood.pl/#menu', selected: false },
    { name: "Rozbrykana Owca", open: [4], id: 5, menu: 'https://www.facebook.com/rozbrykanaowca/photos/a.825362827625171/949163551911764/?type=3&theater', selected: false },
    { name: "Major", open: [1, 2, 3, 4, 5], id: 6, menu: 'https://www.facebook.com/318077918254068/photos/a.1090782617650257/1090782290983623/?type=3&theater', selected: false },
    { name: "OLIMP", open: [1, 2, 3, 4, 5], id: 7, menu: 'http://www.olimprest.pl/restauracje/olimp-krakow-avia-software-park', selected: false },
    { name: "Phuong Dong", open: [1, 2, 3, 4, 5], id: 8, menu: 'https://www.bar-orientalny.pl/pl/bora-komorowskiego-39/menu.html', selected: false },
    { name: "Inny dowóz", open: [0, 1, 2, 3, 4, 5, 6], id: 9, selected: false },
    { name: "Kuchnia (swoje)", open: [0, 1, 2, 3, 4, 5, 6], id: 10, selected: false }
  ];
  openRestaurants = [];

  constructor(private apiClient: ApiClientService, private router: Router) { }

  ngOnInit() {
    this.openRestaurants = this.getOpen(this.allRestaurants);
  }

  private getOpen(restaurants) {
    return restaurants.filter(restaurant => {
      return this.isOpenToday(restaurant);
    });
  }

  private isOpenToday(restaurant) {
    let today = new Date().getDay();
    return restaurant.open.includes(today);
  }

  onChange() {
    if (this.timeRange.from > this.timeRange.to) {
      this.submitDisabled = true;
      return;
    };

    if (!this.openRestaurants.filter(restaurant => { return restaurant.selected }).length) {
      this.submitDisabled = true;
      return;
    }

    this.submitDisabled = false;
  }

  onNavigate(link) {
    window.open(link, "_blank");
  }

  submit() {
    if (this.submitDisabled) return;

    let vote = {
      places: this.openRestaurants
        .filter(restaurant => { return restaurant.selected })
        .map(place => { return place.name }),
      hours: [this.timeRange]
    }

    this.apiClient.addVote(vote).then(() => {
      return this.router.navigate(["/"]);
    }).catch(err => {
      console.error(err);
    });
  }
}
