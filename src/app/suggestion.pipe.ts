import { Pipe, PipeTransform } from '@angular/core';
import { Match } from './models/match.model';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'suggestion'
})
export class SuggestionPipe implements PipeTransform {

  transform(match: Match, args?: any): any {
    let hasCommonPlaces = match.commonPlaces && match.commonPlaces.length;
    let hasCommonTime = match.commonDateRange && match.commonDateRange.length;

    let pos = match.commonPlaces.join(", ").lastIndexOf(', ');
    let places = pos != -1 ? match.commonPlaces.join(", ").substring(0, pos) + " lub " + match.commonPlaces.join(", ").substring(pos + 1) : match.commonPlaces.join(", ");

    let place = hasCommonPlaces ?
      ` do ${places}`
      : `, ale gdzie indziej`;

    let time = hasCommonTime ?
      `o godzinie ${new DatePipe('pl-PL').transform(match.commonDateRange[0], "HH:mm")}`
      : `o innej porze`;

    let text = `${match.users[0].givenName} może iść z Tobą${place} ${time}`;

    return (hasCommonPlaces || hasCommonTime) ? text : null;
  }
}
