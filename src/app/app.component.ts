import { Component } from "@angular/core";
import { LoginService } from "./login.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "wyrocznia obiadowa";
  menuItems = [
    { name: "Jak to działa", link: "/howto" },
    { name: "Historia", link: "/history" }
  ];

  constructor(public login: LoginService) { }
}
